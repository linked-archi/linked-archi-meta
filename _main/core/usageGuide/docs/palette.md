# Building the palette

When we build a palette we need to consider the main [Model Concepts](https://meta.linked.archi/core/#ModelConcept)
* Element
* Relationship or QualifiedRelationship
* Viewpoint

A mentamodel based on Linked.Archi core ontology should use and extend those main classes using rdfs:subClassOf, 
see: [Basic minimal metamodel](https://meta.linked.archi/basic)

## Elements

## Relationships
Relationships can extend the simple arch:Relationship and the more specific arch:QualifiedRelationship.

### Simple relationship
Simple arch:Relationship should also be defined as owl:ObjectProperty(or simply rdf:Property).
This feature is called punning,
where the same subject is interpreted as two fundamentally different types of thing a class and an individual(or property) 
depending on its syntactic context, see:
* https://www.w3.org/TR/owl2-new-features/#F12:_Punning 

and specifically in the contenxt of metamodelling 
* https://www.w3.org/TR/owl2-new-features/#Use_Case_.2315_-_UML_Association_Class_.5BDesigner.5D

other references to metamodelling using this aproach:
https://en.wikipedia.org/wiki/Metaclass_(Semantic_Web)#:~:text=OWL%202%20DL%20supports%20metaclasses,depending%20on%20its%20syntactic%20context.
https://www.mkbergman.com/913/metamodeling-in-domain-ontologies/

This means that in the context of generating a palette the class would be used, while in the case of the model itself
the Relationship will be the rdf:Property inside the statement. In other words those relationships will never be
instanciated inside models. This also means that there is normally not more than 1 relationship.
When metadata is needed to be added to those statements rdf-star can be used; however, 
this option should not be abused and where this metadata can be modelled properly and actually needed in the main
use cases of the model, then arch:QualifiedRelationship should be used instead where cardinality can be restricted using
SHACL if required.


### QualifiedRelationship

Qualified Relationship are also subclasses of :QualifiedRelationship but in conntrast to simple relationships they are 
actualy instances of those classes in the models. This allows for aserting other parameters. 
Similar aproach is used on provenance ontology:
https://www.w3.org/TR/prov-o/#description-qualified-terms


### Relationship wizard
A handy function for a modelling tool is creating a wizard relationship when you drag it from the source element
to the target elemnt the tool should propose possible relationships. 

the properties:
* arch:domainIncludes
* arch:rangeIncludes 

can be used to propose the possible options. In cases there restrictions are available(see restrictions)
then those shall be used to propose valid options to the modeller.

## Visual Notations


