#!/bin/bash

script_name=$0
script_full_path=$(dirname "$0")
widoco="$script_full_path/widoco-1.4.15-jar-with-dependencies.jar"

echo "executing in : $script_full_path"
echo "executing : $widoco $@"
sname="$(basename -s .ttl $1)"
outFolder="../.out/$sname"
echo "outFolder : $outFolder"

java -jar $widoco -ontFile $1 -outFolder $outFolder -getOntologyMetadata -rewriteAll -webVowl -licensius \
-includeImportedOntologies -htaccess -displayDirectImportsOnly

# java -jar $widoco -ontFile $1 -outFolder $outFolder -getOntologyMetadata -rewriteAll -webVowl -licensius -ignoreIndividuals -displayDirectImportsOnly
#java -jar $widoco -ontFile $1 -outFolder .out -getOntologyMetadata -rewriteAll -webVowl
# -includeImportedOntologies -ignoreIndividuals -displayDirectImportsOnly -htaccess
