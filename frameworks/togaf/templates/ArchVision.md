1.	Executive Summary
This document details the Statement of Architecture Work for <insert>.
2.	Version History
Date	Version	Comments
	
3. Purpose
The Statement of Architecture Work defines the scope and approach that will be used to complete an architecture project. The Statement of Architecture Work is typically the document against which successful execution of the architecture project will be measured and may form the basis for a contractual agreement between the supplier and consumer of architecture services. In general, all the information in this document should be at a high level.
It may be that the Statement of Architecture Work is documented using a wiki or as an intranet rather than a text-based document. Even better would be to use a licensed TOGAF tool that captures this output.
This template shows “typical” contents of a Statement of Architecture Work and can be adapted to align with any TOGAF adaptation being implemented.

5.	Statement of Architecture Work Title
6.	Project Request and Background
7.	Project Description and Scope
8.	Overview or Outline of Architecture Vision
9.	Managerial Approach
10.	Change of Scope Procedures
11.	Roles, Responsibilities, and Deliverables
12.	Acceptance Criteria and Procedures
13.	Project Plan and Schedule
14.	Support of the Enterprise Continuum
15.	Signature Approvals
